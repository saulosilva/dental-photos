<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        $validate = [
            'name' => 'required',
            'gender' => 'required'
        ];

        if(request()->has('date_birth')){
            $validate = array_merge($validate, 
                                    [
                                        'date_birth' => 'date|after:-120 years'
                                    ]);
        }

        return $validate;
    }
}
