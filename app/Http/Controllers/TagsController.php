<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;
use Conner\Tagging\Model\Tagged;
use Illuminate\Validation\Rule;

class TagsController extends Controller
{

		private $tags;

		public function __construct(Tag $tags)
		{
			//$this->middleware('auth');
			$this->tags = $tags;

		}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if($request->has('query')){
        $tags = $this->tags->select('id', 'name')->where('name', 'like', '%'.$request->get('query').'%')->limit(10)->get();  
      } else{
        if($request->has('search')){
            $tags = $this->tags->where('name', 'like', '%'.$request->get('search').'%')->paginate(10);  
        } else {
            $tags = $this->tags->paginate(10);  
        }
        
      }

    	return $this->respondOk($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
		    'name' => 'required|unique:tagging_tags',
	    ]);
        $name = $request->get('name');

        $tag = $this->tags->where('name', $name)->first();
        if(!$tag){
            unset($tag);
            $tag = $this->tags;
            $tag->name = $name;
            $tag->save();    
        }
        

    	return $this->respondOk($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('tagging_tags')->ignore($id),
            ]
        ]);
        $tag = $this->tags->find($id);
        
        $slug_old = $tag->slug;

        $tag->name = $request->get('name');
        $tag->save();


        Tagged::where('tag_slug', $slug_old)->update(['tag_name' => $tag->name, 'tag_slug' => $tag->slug ]);

        return $this->respondOk($tag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = $this->tags->find($id);
        
        Tagged::where('tag_slug', $tag->slug)->delete();
        
        $tag->delete();

        return $this->respondOk($tag);
    }
}
