<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PatientPhotosCreateRequest;
use App\Http\Requests\PatientPhotosUpdateRequest;
use App\Contracts\Repositories\PatientPhotosRepository;
use App\Validators\PatientPhotosValidator;
use App\Criteria\SelectPatientCriteria;
use App\Criteria\TagsCriteria;
use App\Criteria\DescriptionCriteria;
use Illuminate\Support\Facades\Storage;


class PatientPhotosController extends Controller
{

    /**
     * @var PatientPhotosRepository
     */
    protected $repository;

    /**
     * @var PatientPhotosValidator
     */
    protected $validator;

    public function __construct(PatientPhotosRepository $repository, PatientPhotosValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(TagsCriteria::class);
        $this->repository->pushCriteria(DescriptionCriteria::class);
        $this->repository->pushCriteria(SelectPatientCriteria::class);

        $patientPhotos = $this->repository
                                          ->with(['patient', 'tagged'])
                                          ->orderBy('patient_photos.id', 'desc')
                                          ->paginate(12);

        return $this->respondOk($patientPhotos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PatientPhotosCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PatientPhotosCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $patientPhoto = $this->repository->create($request->all());

            $response = [
                'message' => 'PatientPhotos created.',
                'data'    => $patientPhoto->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
        
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patientPhoto = $this->repository->find($id);

        return $this->respondOk($patientPhoto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PatientPhotosUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PatientPhotosUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $patientPhoto = $this->repository->update($request->only('photo', 'description'), $id);
    
            $patientPhoto->retag($request->get('tagged'));

            $patientPhoto = $this->repository->with(['patient', 'tagged'])
                                             ->find($id);

            $response = [
                'message' => 'PatientPhotos updated.',
                'data'    => $patientPhoto->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = $this->repository->find($id);
        $file_exist = Storage::exists('photos/'.$photo->photo);

        if($file_exist){
            //Storage::delete('photos/'.$photo->photo);
        }

        $deleted = $photo->delete();

        return $this->respondOk([
            'message' => 'PatientPhotos deleted.',
            'deleted' => $deleted,
        ]);
    }

    /**
     * Upload photos paciente.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('photos')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $patient_id = $request->get('patient_id');

        $avatar = $request->photos;
        $save = $avatar->store('photos');

        $filename = str_replace('photos/', '', $save);
        if($request->has('photo_id')){
            $patientPhoto = $this->repository->update([
                                                'photo'         => $filename, 
                                            ], $request->get('photo_id'));
        } else {
            $patientPhoto = $this->repository->create([
                                                    'patient_id'    => $patient_id, 
                                                    'photo'         => $filename, 
                                                    'description'   => ''
                                                ]);    
        }
        
        
        return $this->respondOk(['photo' => $filename, 'id' => $patientPhoto->id]);
    }
}
