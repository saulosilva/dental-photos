<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PatientCreateRequest;
use App\Http\Requests\PatientUpdateRequest;
use App\Contracts\Repositories\PatientRepository;
use App\Validators\PatientValidator;


class PatientsController extends Controller
{

    /**
     * @var PatientRepository
     */
    protected $repository;

    /**
     * @var PatientValidator
     */
    protected $validator;

    public function __construct(PatientRepository $repository, PatientValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $patients = $this->repository->orderBy('id', 'desc')->paginate(10,['id', 'system_id', 'name', 'date_birth', 'gender', 'avatar']);

        if (request()->wantsJson()) {
            return $this->respondOk($patients);
        }

        return view('patients', ['title' => 'Pacientes', 'patients' => $patients]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PatientCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PatientCreateRequest $request)
    {

        try {
            
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            
            $inputs = $request->all();
            if($inputs['date_birth'] === ""){
                unset($inputs['date_birth']);
            }

            $patient = $this->repository->create($inputs);

            $response = [
                'message' => 'Patiente criado!',
                'data'    => $patient->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PatientUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PatientUpdateRequest $request, $id)
    {

        try {
            
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $inputs = $request->all();
            if($inputs['date_birth'] === ""){
                unset($inputs['date_birth']);
            }
            $patient = $this->repository->update($inputs, $id);

            $response = [
                'message' => 'Patiente editado!',
                'data'    => $patient->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        $response = [
                'message' => 'Patiente apagado!',
                'deleted' => $deleted,
            ];

        return $this->respondOk($response);
    }

    /**
     * Upload image profile.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function avatar(Request $request)
    {
        if (!$request->hasFile('avatar')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $avatar = $request->avatar;
        $save = $avatar->store('avatars');

        $filename = str_replace('avatars/', '', $save);
        
        return $this->respondOk(['image' => $filename]);
    }
}
