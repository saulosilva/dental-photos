<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind(\App\Contracts\Repositories\PatientRepository::class, \App\Repositories\Eloquent\PatientRepositoryEloquent::class);
        \App::bind(\App\Contracts\Repositories\PatientPhotosRepository::class, \App\Repositories\Eloquent\PatientPhotosRepositoryEloquent::class);
    }
}
