<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class SelectPatientCriteria
 * @package namespace App\Criteria;
 */
class SelectPatientCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $patient_id = request()->get('patient_id', null);
        if(request()->has('patient_id') && $patient_id > 0){
            $model->where('patient_id', (int)$patient_id);
        }

        return $model;
    }
}
