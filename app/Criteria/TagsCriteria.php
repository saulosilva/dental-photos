<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
use Conner\Tagging\Util;
/**
 * Class TagsCriteria
 * @package namespace App\Criteria;
 */
class TagsCriteria implements CriteriaInterface
{

    private $groups = [];

    private $total_groups = 0;
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $search  = request()->get('search', null);

        if($search){  
            
            $search = collect($search);
            $search = $search->map(function($item){
                return strtoupper($item);
            });
            
            if(($search->contains('AND') == false) && ($search->contains('OR') == false) && ($search->contains('NOT') == false) ){
                $filtered = $search->filter(function ($value) {
                    return $value != '(' && $value != ')';
                });                
                $model->withAnyTag($filtered->all());
            } else {

                $format_input = $this->formatInputSearch($search->all());

                $condition = 'where'; // where or orWhere
                foreach ($format_input as $auxArr) {
                    $arr = collect($auxArr);
                    if($arr->count() == 1){
                        $condition = $arr[0] == 'AND' ? 'where' : 'orWhere';
                    } else {
                        $model->{$condition}(function($q) use ($arr){
                            if($arr->contains('AND') && !$arr->contains('NOT')){
                                $search_slug = $this->formatSearchAnd($arr);
                                $q->withAllTags($search_slug);
                            } elseif($arr->contains('OR') && !$arr->contains('NOT')) {
                                $search_slug = $this->formatSearchOr($arr);
                                $q->withAnyTag($search_slug);
                            } elseif($arr->contains('NOT')) {
                                $search_slug = $this->formatSearchNot($arr);
                                if($arr->contains('AND')){
                                    $q->withNotAllTags($search_slug);
                                } else {
                                    $q->withNotAnyTags($search_slug);
                                }
                            } else {
                                $search_slug = $this->formatSearchAll($arr);
                                $q->withAnyTag($search_slug);
                            }
                        });
                    }
                    
                }
            }
            
        }
        return $model;
    }

    public function formatSearchAll($search)
    {
        $query = [];
        
        $query = $search->filter(function($tag, $index) {
            return strtoupper($tag) != '(' && strtoupper($tag) != ')';
        });

        return $query->all();
    }

    public function formatSearchOr($search)
    {
        $query = [];
        
        $query = $search->filter(function($tag, $index) {
            return strtoupper($tag) != 'OR' && strtoupper($tag) != '(' && strtoupper($tag) != ')';
        });

        return $query->all();
    }

    public function formatSearchAnd($search)
    {
        $query = [];
        
        $query = $search->filter(function($tag, $index) {
            return strtoupper($tag) != 'AND' && strtoupper($tag) != '(' && strtoupper($tag) != ')';
        });

        return $query->all();
    }

    public function formatSearchNot($search)
    {
        $query = [];
        
        $query = $search->filter(function($tag, $index) {
            $tag = strtoupper($tag);
            return $tag != 'NOT' && 
                   $tag != 'AND' && 
                   $tag != 'OR' && 
                   $tag != '(' && 
                   $tag != ')';
        });

        return $query->all();
    }

    private function formatInputSearch($search)
    {
        $a1 = collect($search);

        if ($a1->count() === 0) { // our base case
             return $this->groups;
        } else {
            $aux = $a1->first();

            if($aux == ')'){
                $this->groups[$this->total_groups][] = $aux;
                $separator = $a1->slice(1)->first();
                if($separator == 'AND' || $separator == 'OR'){
                    $this->total_groups++;// cria um grupo para o separador AND ou OR
                    $this->groups[$this->total_groups][] = $separator;
                }
                $a1->shift();
                $this->total_groups++;
            } else {                
                $this->groups[$this->total_groups][] = $aux;
            }

            $a1->shift();

            return $this->formatInputSearch($a1->all());         
        }
    }
}
