<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DescriptionCriteria
 * @package namespace App\Criteria;
 */
class DescriptionCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $search  = request()->get('search', null);
        if($search){
            if(is_array($search))
            {  
                $collect = collect($search);
                $search = $collect->map(function($tag){
                    return (strtoupper($tag) == 'AND' || 
                            strtoupper($tag) == 'OR' || 
                            strtoupper($tag) == 'NOT') 
                            ? strtoupper($tag) : $tag;
                });

                if(($search->contains('AND') == false) && ($search->contains('OR') == false) && ($search->contains('NOT') == false) ){
                    $model->orWhere('description', 'LIKE','%'.$search->implode(' ').'%');
                }
            }
        }
        return $model;
    }
}
