<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PatientRepository
 * @package namespace App\Contracts\Repositories;
 */
interface PatientRepository extends RepositoryInterface
{
    //
}
