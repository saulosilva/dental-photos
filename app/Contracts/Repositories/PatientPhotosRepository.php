<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PatientPhotosRepository
 * @package namespace App\Contracts\Repositories;
 */
interface PatientPhotosRepository extends RepositoryInterface
{
    //
}
