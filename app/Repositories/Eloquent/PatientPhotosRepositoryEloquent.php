<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\PatientPhotosRepository;
use App\Models\PatientPhoto;
use App\Validators\PatientPhotosValidator;

/**
 * Class PatientPhotosRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class PatientPhotosRepositoryEloquent extends BaseRepository implements PatientPhotosRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PatientPhoto::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PatientPhotosValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
