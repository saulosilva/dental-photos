<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\PatientRepository;
use App\Models\Patient;
use App\Validators\PatientValidator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class PatientRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class PatientRepositoryEloquent extends BaseRepository implements PatientRepository, CacheableInterface
{
    use CacheableRepository;

    protected $fieldSearchable = [
        'system_id'=>'=',
        'name'=>'like',
        //'photos.tagged.tag_name'=>'like',
        //'photos.description'=>'like'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Patient::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PatientValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
