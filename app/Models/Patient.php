<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Conner\Tagging\Taggable;

class Patient extends Model implements Transformable
{
 		use TransformableTrait, Taggable;

    protected $fillable = [
        'system_id',
    	'name',
    	'date_birth',
    	'gender',
    	'avatar',
    ];

    protected $dates = ['date_birth'];

    public function photos()
    {
    	return $this->hasMany('App\Models\PatientPhoto')->with('tagged');
    }

    public function getAvatarAttribute()
    {
        return ($this->attributes['avatar']=== '') ? 'no-image.jpg' : $this->attributes['avatar'];
    }
}
