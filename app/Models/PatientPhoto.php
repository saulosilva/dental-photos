<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\Taggable;
use Conner\Tagging\Model\Tagged;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PatientPhoto extends Model implements Transformable
{
    use Taggable, TransformableTrait;    

    protected $fillable = [
    	'patient_id',
    	'photo',
        'description'
    ];

    public function patient()
    {
    	return $this->belongsTo('App\Models\Patient');
    }

    /**
     * Filter model to subset with the given tags
     *
     * @param $tagNames array|string
     */
    public function scopeWithNotAllTags($query, $tagNames)
    {
        if(!is_array($tagNames)) {
            $tagNames = func_get_args();
            array_shift($tagNames);
        }
        
        $tagNames = static::$taggingUtility->makeTagArray($tagNames);
        
        $normalizer = config('tagging.normalizer');
        $normalizer = $normalizer ?: [static::$taggingUtility, 'slug'];
        $className = $query->getModel()->getMorphClass();

        $allTags = [];
        foreach($tagNames as $tagSlug) {
            $tags = Tagged::where('tag_slug', call_user_func($normalizer, $tagSlug))
                ->where('taggable_type', $className)
                ->pluck('taggable_id');

            $primaryKey = $this->getKeyName();
            $query->whereNotIn($this->getTable().'.'.$primaryKey, $tags);
        }
        
        return $query;
    }


    /**
     * Filter model to subset with the given tags
     *
     * @param $tagNames array|string
     */
    public function scopeWithNotAnyTags($query, $tagNames)
    {
        if(!is_array($tagNames)) {
            $tagNames = func_get_args();
            array_shift($tagNames);
        }
        
        $tagNames = static::$taggingUtility->makeTagArray($tagNames);
        
        $normalizer = config('tagging.normalizer');
        $normalizer = $normalizer ?: [static::$taggingUtility, 'slug'];
        
        $tagNames = array_map($normalizer, $tagNames);
        $className = $query->getModel()->getMorphClass();
        
        $tags = Tagged::whereIn('tag_slug', $tagNames)
            ->where('taggable_type', $className)
            ->pluck('taggable_id');

        $primaryKey = $this->getKeyName();
        return $query->whereNotIn($this->getTable().'.'.$primaryKey, $tags);
    }
}

