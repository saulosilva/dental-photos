<?php

use App\Models\Patient;
use App\Models\PatientPhoto;
use Conner\Tagging\Model\Tag;
use App\User;

class ApplicationTestCase extends TestCase
{

	public function createUser($qtd = 1, $attrs = [])
	{
		$user = factory(User::class, $qtd)
      						->create($attrs);

		return $user;
	}
	
	public function createPatient($qtd = 1, $attrs = [])
	{
		$patient = factory(Patient::class, $qtd)
      						->create($attrs);

		return $patient;
	}

	public function createTag($qtd = 1, $attrs = [])
	{
		$tag = factory(Tag::class, $qtd)
      						->create($attrs);

		return $tag;
	}

	protected function login(){
		$user = $this->createUser();
		
		Auth::loginUsingId($user->id);
	}
}