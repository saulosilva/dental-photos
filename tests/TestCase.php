<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{

    protected $useDatabaseTransactions = true;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost/';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        if($this->useDatabaseTransactions){
            $this->setDatabaseTransactions();
        }

        return $app;
    }

    protected function setDatabaseTransactions()
    {
        app('db')->beginTransaction();
        $this->beforeApplicationDestroyed(function(){
            $this->app->make('db')->rollBack();
        });


    }
}
