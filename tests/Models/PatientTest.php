<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Patient;
use App\Models\PatientPhoto;
use Conner\Tagging\Model\Tag;

class PatientTest extends ApplicationTestCase
{
	
	public function testCreatePatient()
	{
		$patient = $this->createPatient();

		$patient_id = $patient->id;

		$this->assertInstanceOf(Patient::class, $patient);

		$this->assertEquals($patient->id, $patient_id);
	}

	public function testCreatePatientPhoto()
	{
		$patient = $this->createPatient();

		$patient_photo = $patient->photos()->create(['photo' => $patient->avatar, 'description' => 'description text']);

		$this->assertInstanceOf(Patient::class, $patient);
		$this->assertInstanceOf(PatientPhoto::class, $patient_photo);
		$this->assertEquals($patient->id, $patient_photo->patient_id);
	}


	public function testCreatePatientPhotoTags()
	{
		$patient = $this->createPatient();

		$patient_photo = $patient->photos()->create(['photo' => $patient->avatar, 'description' => 'description text']);

		$tags = $patient_photo->tag('tag 1,tag 2');
		$patient_photo->fresh();
		
		$tags = $patient_photo->existingTags();		

		$this->assertCount(2, $tags->toArray());
	}

	public function testCreatePatientPhotoTagsRelatedDelete()
	{
		$patient = $this->createPatient();

		$patient_photo = $patient->photos()->create(['photo' => $patient->avatar, 'description' => 'description text']);

		$tags = $patient_photo->tag('tag 1,tag 2');
		$patient_photo->untag('tag 1');

		$patient_photo->fresh();
		
		$tags = $patient_photo->existingTags();		

		$this->assertCount(1, $tags->toArray());
	}

	public function testCreatePatientPhotoTagsDelete()
	{
		$patient = $this->createPatient();

		$patient_photo = $patient->photos()->create(['photo' => $patient->avatar, 'description' => 'description text']);

		$tags = $patient_photo->tag('tag 1,tag 2');

		$tag = Tag::where('slug', 'tag-1')->first();
		$tag->delete();

		$patient_photo->fresh();
		
		$tags = $patient_photo->existingTags();		

		$this->assertCount(1, $tags->toArray());
	}
}