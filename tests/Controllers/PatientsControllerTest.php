<?php


use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Conner\Tagging\Model\Tag;
use App\Models\Patient;

/**
* 
*/
class PatientsControllerTest extends ApplicationTestCase
{
	
	function test_home_patient_page()
	{
		$this->login();

		$this->visit('/home')
			   ->see('Pacientes');
	}

	function test_home_patient_api()
	{
		$this->login();

		$this->get('/api/patients');
	}

  function test_home_patient_create_api()
	{
		$this->login();

		$this->post('/api/patients', [
				'name' 				=> 'Test name',
				'date_birth'	=> Carbon::now()->subYears(18),
				'gender' 			=> 'M',
				'avatar' 			=> 'image/avatar.jpg'
			])
		->assertResponseOk();

		// validate teste
		$this->post('/api/patients', [
				'name' 				=> '',
				'date_birth'	=> '',
				'gender' 			=> 'M',
				'avatar' 			=> 'image/avatar.jpg'
			])
		->assertResponseStatus(302);
	}

 	function test_home_patient_update_api()
	{
		$this->login();

		$patient = $this->createPatient(1, ['name' => 'name 1']);

		$this->put('/api/patients/'.$patient->id, [
				'name' 				=> 'name 2',
				'date_birth'	=> Carbon::now()->subYears(18),
				'gender' 			=> 'M',
				'avatar' 			=> 'image/avatar.jpg'
			])
		->assertResponseOk();

		$patient_update = Patient::find($patient->id);

		$this->assertTrue(($patient_update->name != $patient->name));

		// validate teste
		$this->put('/api/patients/'.$patient->id, [
				'name' 				=> '',
				'date_birth'	=> '',
				'gender' 			=> 'M',
				'avatar' 			=> 'image/avatar.jpg'
			])
		->assertResponseStatus(302);
	}

	function test_home_patient_delete_api()
	{
		$this->login();

		$patient = $this->createPatient();

		$patient_photo = $patient->photos()
														 ->create(['photo' => $patient->avatar, 'description' => 'description text']);
		$patient_photo->tag(['tag1', 'tag2']);	

		$this->delete('/api/patients/'.$patient->id)
				 ->assertResponseOk();

		$this->assertCount(2, Tag::get()->toArray());
	}
}