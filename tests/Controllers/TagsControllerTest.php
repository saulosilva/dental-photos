<?php

use Conner\Tagging\Model\Tag;

class TagsControllerTest extends ApplicationTestCase
{
	function test_tags_api()
	{
		$this->login();

		$this->get('/api/tags')
			    ->assertResponseOk();
	}

	function test_tag_create_api()
	{
		$this->login();

		$this->post('/api/tags', [
				'name' => 'Test name'
			])
		->assertResponseOk();

		$tags = Tag::get();

		$this->assertCount(1, $tags->toArray());

	}

	function test_tag_update_api()
	{
		$this->login();

		$tag = $this->createTag(1, ['name' => 'tag 1']);

		$this->put('/api/tags/'.$tag->id, [
				'name' => 'Tag 2'
			])
		->assertResponseOk();

		$tag_update = Tag::find($tag->id);

		$this->assertTrue(($tag_update->name != $tag->name));

	}

	function test_tag_delete_api()
	{
		$this->login();

		$tag = $this->createTag();

		$this->delete('/api/tags/'.$tag->id)
				 ->assertResponseOk();

		$tag = Tag::find($tag->id);

		$this->assertNull($tag);

	}


	function test_tag_delete_related_api()
	{
		$this->login();

		$patient = $this->createPatient();

		$patient_photo = $patient->photos()
														 ->create(['photo' => $patient->avatar, 'description' => 'description text']);

	  $patient_photo->tag('test,test1');

	  $tag = $patient_photo->tags->first();

		$this->delete('/api/tags/'.$tag->id)
				 ->assertResponseOk();

		$tag = Tag::find($tag->id);

		$this->assertNull($tag);

	}

}