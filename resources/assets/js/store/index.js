
import Vue from 'vue'
import Vuex from 'vuex'


import patients from './modules/patients'
import patient_photos from './modules/patient_photos'

import * as actions from './actions'


Vue.use(Vuex)

const debug = true

export default new Vuex.Store({
	actions,
  modules: {
    patients,
    patient_photos
  },
  strict: debug
})