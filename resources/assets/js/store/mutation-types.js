/*
	PATIENTS
*/
export const RECEIVE_PATIENTS = 'RECEIVE_PATIENTS'
export const SEARCH_PATIENTS 	= 'SEARCH_PATIENTS'
export const UPDATE_PATIENT 	= 'UPDATE_PATIENT'
export const DELETE_PATIENT 	= 'DELETE_PATIENT'


export const MORE_PATIENTS  = 'MORE_PATIENTS'
export const SELECT_PATIENT = 'SELECT_PATIENT'
export const UNSELECT_PATIENT = 'UNSELECT_PATIENT'

export const SET_PAGE 			= 'SET_PAGE'
export const SET_LOADING 		= 'SET_LOADING'
export const AVATAR_PATIENT = 'AVATAR_PATIENT'
export const VISIBLE 				= 'VISIBLE'


export const CLEAR  	 			= 'CLEAR'
/*
	PATIENT PHOTOS
*/

export const RECEIVE_PHOTOS  = 'RECEIVE_PHOTOS'
export const SEARCH_PHOTOS   = 'SEARCH_PHOTOS'
export const GET_PHOTO  	   = 'GET_PHOTO'
export const UPDATE_PHOTO  	 = 'UPDATE_PHOTO'
export const CLEAR_PHOTOS  	 = 'CLEAR_PHOTOS'
export const VISIBLE_PHOTOS  = 'VISIBLE_PHOTOS'
export const MORE_PHOTOS 	   = 'MORE_PHOTOS'
export const SET_PAGE_PHOTOS = 'SET_PAGE_PHOTOS'


