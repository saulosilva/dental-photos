import * as types from '../mutation-types'
import _ from 'lodash'

const state = {
    patients: [],
    more_patients: false,
    page: 1,
    search: '',
    visible: true,
    patient: { name: '', system_id: '', gender: 'M', date_birth: '', id: '', avatar: 'no-image.jpg' }
}

/**
    GETTERS
*/
const getters = {
  patients: state => state.patients,
  page: state => state.page,
  search: state => state.search,
  visible: state => state.visible,
  patient: state => state.patient,
  more_patients: state => state.more_patients
}


/**
    ACTIONS
*/
const actions = {

    getPatients ({ commit }) {
        
        return Vue.http.get('/api/patients', {
                params: {
                    page: state.page, 
                    search: state.search
                }
            });
    },

    onInfinite ({ dispatch, commit }, clear = false){
        if(clear){
            commit(types.CLEAR)
            commit(types.UNSELECT_PATIENT)
        }
        commit(types.VISIBLE, true)

        setTimeout(() => {
            dispatch('getPatients').then(res => {
                const patients = res.data.data
                if(patients.data.length){
                    commit(types.RECEIVE_PATIENTS, patients.data)
                    commit(types.SET_PAGE)
                    commit(types.MORE_PATIENTS, (patients.current_page < patients.last_page))
                } else{
                    commit(types.MORE_PATIENTS, false)
                    commit(types.VISIBLE, false)    
                }
            })
        }, 1)
    },

    searchPatients ({ dispatch, commit }, search){
        commit(types.SEARCH_PATIENTS, search)
        dispatch('onInfinite')
    },

    storePatient({dispatch, commit}, patient){ 
        return Vue.http.post('/api/patients', patient).then(res => {
            var patient = res.data.data
            dispatch('searchPatients', '')
        });
    },

    updatePatient({dispatch, commit}, patient){ 
        return Vue.http.put('/api/patients/'+patient.id, patient).then(res => {
            var response = res.data.data
            var patient = response.data
            commit(types.UPDATE_PATIENT, patient)
        });
    },

    deletePatient({dispatch, commit}, patient_id){ 
        Vue.http.delete('/api/patients/'+patient_id).then(res => {
            var patient = res.data.data
            
            commit(types.SEARCH_PATIENTS, '')
            
            dispatch('onInfinite')
        });
    },

    changeAvatar ({ commit }, avatar) {
        commit(types.AVATAR_PATIENT, avatar)
    }
}


/**
    MUTATIONS
*/
const mutations = {
    [types.RECEIVE_PATIENTS] (state, patients) {
        var n = _.map(patients, function(p){
            return {
                'id': p.id, 
                'system_id': p.system_id,
                'name': p.name, 
                'avatar': p.avatar,
                'gender': p.gender,
                'date_birth':  p.date_birth,
                'selected': false
            }
        }); 
        state.patients = [...state.patients, ...n]
        state.visible = false
    },

    [types.SEARCH_PATIENTS] (state, search) {
        state.page = 1
        state.patients = []
        state.search = search
    },

    [types.CLEAR] (state, search) {
        state.page = 1
        state.patients = []
        state.search = search
    },

    // select patient
    [types.SELECT_PATIENT] (state, id){
        state.patients.forEach(patient => {
            patient.selected = false;
            if(patient.id === id){
                patient.selected = true
            }
        });
    },
    [types.UNSELECT_PATIENT] (state){
        state.patients.forEach(patient => {
            patient.selected = false;
        });
    },

    [types.UPDATE_PATIENT] (state, patient) { 
        var id = state.patients.find(p => { return p.id == patient.id })
        var index = state.patients.indexOf(id)
        patient.selected = true
        state.patients[index] = patient
        state.patients = [...state.patients, {}]
        var patients = _.remove(state.patients, p => {            
              return p.id;
            })

        state.patients = patients
    },

    // mutate variables
    [types.MORE_PATIENTS] (state, more) {
        state.more_patients = more;
    },
    [types.AVATAR_PATIENT] (state, avatar) {
        state.patient.avatar = avatar;
    },
    [types.SET_PAGE] (state) {
        state.page++;    
    },
    [types.VISIBLE] (state, visible) {
        state.visible = visible;
    }
}


export default {
    state,
    getters,
    mutations,
    actions
}