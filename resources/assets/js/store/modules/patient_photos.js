import * as types from '../mutation-types'
import _ from 'lodash'

/*
patient_id
photos
get all photos 
get all photos patient_id
*/

const state = {
    photos: [],
    patient_id: 0,
    photo: {},
    page_photos: 1,
    search_photos: [],
    visible_photos: true,
    more_photos: false
}

/**
    GETTERS
*/
const getters = {
  photos: state => state.photos,
  patient_id: state => state.patient_id,
  page_photos: state => state.page_photos,
  search_photos: state => state.search_photos,
  visible_photos: state => state.visible_photos,
  more_photos: state => state.more_photos
}

const actions = {

    getPhotos ({ commit }) {

        return Vue.http.get('/api/patient-photos', {
                params: {
                    page: state.page_photos, 
                    patient_id: state.patient_id,
                    search: state.search_photos
                }
            });
    },

    onInfinitePhotos ({ dispatch, commit }, clear = false){
        if(clear){
            commit(types.CLEAR_PHOTOS)
        }
        
        commit(types.VISIBLE_PHOTOS, true)

        setTimeout(() => {
            dispatch('getPhotos').then(res => {
                var photos = res.data.data
                
                if(photos.data.length){
                    commit(types.RECEIVE_PHOTOS, photos.data)    
                    commit(types.SET_PAGE_PHOTOS)
                    commit(types.MORE_PHOTOS, (photos.current_page < photos.last_page))
                } else{
                    commit(types.MORE_PHOTOS, false)
                    commit(types.VISIBLE_PHOTOS, false)    
                }
            })  
        }, 1000)
        
    },

    searchPhotos ({ dispatch, commit }, payload){

        commit(types.SEARCH_PHOTOS, payload)
        if(payload.on_search){
            setTimeout(() => {
                dispatch('onInfinitePhotos', true)
            }, 1000)    
        }
        
    },

    updatePhoto({ commit }, photo) {
        Vue.http.put('/api/patient-photos/'+photo.id, photo).then(res => {
            var response = res.data.data
            
            commit(types.UPDATE_PHOTO, response.data)
        })
    },

    deletePhoto({dispatch, commit}, photo_id){ 
        commit('SEARCH_PHOTOS', {search: {}, 'remove': 0});

        return Vue.http.delete('/api/patient-photos/'+photo_id);
    },

}

/**
    MUTATIONS
*/
const mutations = {
    [types.RECEIVE_PHOTOS] (state, photos) {
        state.photos = [...state.photos, ...photos]
        state.visible_photos = false
    },

    [types.SEARCH_PHOTOS] (state, payload) {
        var id = _.findIndex(state.search_photos, (o) => { return o == payload.search });;

        if(id > -1 && payload.remove){
            var search = _.remove(state.search_photos, (n) => { return !(n === payload.search) });
            state.search_photos = search;
        } else {
            if(payload.search != '')
                state.search_photos = [...state.search_photos, ...payload.search]
        }
        
    },

    [types.UPDATE_PHOTO] (state, photo) {        
        var id = state.photos.find(p => { return p.id == photo.id })
        var index = state.photos.indexOf(id)
        photo.selected = true
        state.photos[index] = photo
        state.photos = [...state.photos, {}]
        var photos = _.remove(state.photos, p => {            
              return p.id;
            })

        state.photos = photos
    },

    // mutate variables    
    [types.MORE_PHOTOS] (state, more) {
        state.more_photos = more;
    },
    [types.SET_PAGE_PHOTOS] (state) {
        state.page_photos++;    
    },
    [types.VISIBLE_PHOTOS] (state, visible) {
        state.visible_photos = visible;
    },
    [types.SELECT_PATIENT] (state, patient_id) {
        state.patient_id = patient_id;
        state.page_photos = 1
        state.photos = []
    },
    [types.UNSELECT_PATIENT] (state) {
        state.patient_id = 0
        state.page_photos = 1
        state.photos = []
    },
    [types.CLEAR_PHOTOS] (state, patient_id) {
        state.page_photos = 1
        state.photos = []
    }
}



export default {
    state,
    getters,
    mutations,
    actions
}
