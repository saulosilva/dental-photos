import * as types from './mutation-types'

export const selectPatient = ({ dispatch, commit }, id) => {
  commit(types.SELECT_PATIENT, id)
  dispatch('onInfinitePhotos', true)
}

export const unselectPatient = ({ dispatch, commit }) => {
  commit(types.UNSELECT_PATIENT)
  dispatch('onInfinitePhotos', true)
}