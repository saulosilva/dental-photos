
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

require('js-cookie');
require('jquery-slimscroll');
require('bootstrap-switch');
require('select2');
require('jquery-validation');
require('bootstrap-tagsinput');
require('typeahead.js');
require('jquery.maskedinput/src/jquery.maskedinput.js');


window.Bloodhound = require('bloodhound-js');
window.toastr = require('toastr');

require('../../../public/assets/global/plugins/jquery.blockui.min.js');

define('../../../public/assets/global/scripts/app.js');
define('../../../public/assets/layouts/layout3/scripts/layout.js');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
require('vue-resource');
require('sweetalert2');

window.moment = require('moment')
require('moment/locale/pt-br')

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import VueClip from 'vue-clip'
Vue.use(VueClip)

// formatSize filter
Vue.filter('formatSize', function(size) {
  if (size > 1024 * 1024 * 1024 * 1024) {
    return (size / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB';
  } else if (size > 1024 * 1024 * 1024) {
    return (size / 1024 / 1024 / 1024).toFixed(2) + ' GB';
  } else if (size > 1024 * 1024) {
    return (size / 1024 / 1024).toFixed(2) + ' MB';
  } else if (size > 1024) {
    return (size / 1024).toFixed(2) + ' KB';
  }
  return size.toString() + ' B';
});

// reverse
Vue.filter('reverse', function(value) {
  // slice to make a copy of array, then reverse the copy
  return value.slice().reverse();
});

// parseInt
Vue.filter('parseInt', function(value) {
  return parseInt(value);
});

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);
    //request.headers.set('Authorization', 'Bearer 2eab5758fd5af73040b46258b5a2968684dee540a657163e0856027ba482992deb8a39cb212cae9d');
    

    next();
});
Vue.http.options.before = (request) => {
  // abort previous request, if exists

  if (this.previousRequest) {
    this.previousRequest.abort()
  }
  // set previous request on Vue instance
  this.previousRequest = request
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
