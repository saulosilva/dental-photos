import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Patient from './components/patients/Patient.vue'
import Tags from './components/tags/Tags.vue'

const routes = [
  { path: '/', component: Patient },
  { path: '/tags', component: Tags }
]

export default new VueRouter({
  routes
})