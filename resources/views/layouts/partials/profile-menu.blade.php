<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <li>
            <router-link to="/">
                <i class="icon-people"></i> Pacientes
            </router-link>
        </li>
        <li>
            <router-link to="/tags">
                <i class="icon-tag"></i> Tags
            </router-link>
        </li>
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" class="img-circle" src="/assets/layouts/layout3/img/avatar9.jpg">
                <span class="username username-hide-mobile">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    
                </li>
                <li>
                    <a href="{{ url('/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        
                        <i class="icon-key"></i> Sair</a>
                </li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
    </ul>
</div>