@extends('layouts.login')

@section('content')
<div class="content">
    <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <h3 class="form-title">Acessar Painel</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Preencha seu e-mail e senha. </span>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">E-mail</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" 
                       type="text" 
                       autocomplete="off" 
                       placeholder="E-mail" 
                       value="{{ old('email') }}" 
                       name="email"
                       autofocus /> 
            </div>
            @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Senha</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" 
                       type="password" 
                       autocomplete="off" 
                       placeholder="Senha" 
                       name="password" /> 
            </div>
            @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
        </div>
        <div class="form-actions">
            <label class="rememberme mt-checkbox mt-checkbox-outline">
                <input type="checkbox"
                       name="remember" 
                       value="1" 
                       {{ old('remember') ? 'checked' : ''}} 
                 /> Lembrar de mim
                <span></span>
            </label>
            <button type="submit" class="btn green pull-right"> Entrar </button>
        </div>
    </form>
</div>
@endsection
