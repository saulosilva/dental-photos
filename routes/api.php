<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/patients/avatar', 'PatientsController@avatar');
Route::resource('/patients', 'PatientsController', ['except' => ['show', 'edit', 'create']]);

Route::post('/patient-photos/upload', 'PatientPhotosController@upload');
Route::resource('/patient-photos', 'PatientPhotosController', ['except' => ['show', 'edit', 'create']]);
Route::resource('/tags', 'TagsController', ['except' => ['show', 'edit', 'create']]);
