<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
	protected $faker;
		public function __construct(Generator $faker){
			$this->faker = $faker;
		}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // remove images
        //Storage::deleteDirectory('avatars');
        //Storage::deleteDirectory('photos');
        //create folder
        //Storage::makeDirectory('avatars');
        //Storage::makeDirectory('photos');

    	factory(App\User::class, 1)->create(['email' => 'admin@admin.com', 'name' => 'Administrador']);
    	$patients = factory(App\Models\Patient::class, 20)->create(['system_id' => 0]);
        
    	$patients->each(function($patient){
    		$photos = factory(App\Models\PatientPhoto::class, 5)->create(['patient_id' => $patient->id]);
    		$photos->each(function($photo){
    			$photo->tag($this->faker->words(5));
    		});
    	});

        //DB::update("UPDATE patient_photos SET photo = replace(photo, './storage/app/photos/', '') WHERE id != 0", []);
        //DB::update("UPDATE patients SET avatar = replace(avatar, './storage/app/avatars/', '') WHERE id != 0", []);
        // $this->call(UsersTableSeeder::class);
    }
}
