<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PatientPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')
                  ->unsigned();
            $table->string('photo');
            $table->text('description')
                 ->nullable();
            $table->timestamps();

            //related
            $table->foreign('patient_id')
                  ->references('id')->on('patients')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_photos');
    }
}
