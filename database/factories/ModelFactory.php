<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'api_token' => str_random(60),
    ];
});

$factory->define(App\Models\Patient::class, function (Faker\Generator $faker) {
    return [
        'name' 			 => $faker->name,
    	  'date_birth' => $faker->date('Y-m-d', '-18 years'),
    		'gender' 		 => $faker->randomElement(['M', 'F']),
    		//'avatar' 		 => $faker->image('./storage/app/avatars', 150, 150, 'people'),
        'avatar'     => 'team'.rand(1, 16).'.jpg'
    ];
});

$factory->define(App\Models\PatientPhoto::class, function (Faker\Generator $faker) {
    return [
    		//'photo'       => $faker->image('./storage/app/photos', 500, 500, 'abstract'),
        'photo'       => rand(1, 60).'.jpg',
        'description' => $faker->text,
    ];
});

$factory->define(Conner\Tagging\Model\Tag::class, function (Faker\Generator $faker) {
    return [
    		'name' => ucfirst($faker->word)
    ];
});
