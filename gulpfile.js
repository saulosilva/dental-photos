const elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    /* webpack/vuejs JS */
    mix.sass([
    		'app.scss',
    		//'bootstrap.scss',
    		
            'global/components.scss',
    		'global/plugins.scss',
    		//'apps/*.scss',
    		//'pages/*.scss',
    		'layouts/layout3/layout.scss',
    		'layouts/layout3/themes/default.scss',
    		'layouts/layout3/custom.scss',
    	])
    	 .webpack([   		
        'app.js'
      ], 'public/js/app.js');

     mix.version(['css/app.css', 'js/app.js']);

});

